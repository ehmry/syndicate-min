# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[streams, os],
  min,
  ./min_preserves,
  ./min_syndicate

proc main =
  var params = commandLineParams()
  if params.len != 1:
    quit "expected a path to a script as a single argument"
  var path = params[0]
  var i = newMinInterpreter(path, parentDir path)
  i.preserves_module()
  i.syndicate_module()
  i.interpret(newFileStream(path, fmRead))

main()
