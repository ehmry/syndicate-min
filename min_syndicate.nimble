# Package
version = "20240522"
author        = "Emery Hemingway"
description   = "Syndicate bindings for the Min language"
license       = "unlicense"
bin           = @["mindicate"]
srcDir        = "pkg"

requires "http://git.syndicate-lang.org/ehmry/syndicate-nim.git >= 20240422", "checksums", "minline >= 0.1.1 & < 0.2.0", "nimquery >= 2.0.1 & < 3.0.0", "zippy >= 0.5.6 & < 0.6.0"
